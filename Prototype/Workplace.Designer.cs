﻿namespace Prototype
{
    partial class Workplace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Workplace));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend4 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.UsDateTB = new System.Windows.Forms.TextBox();
            this.Date = new System.Windows.Forms.Label();
            this.UsQuantityTB = new System.Windows.Forms.ComboBox();
            this.DGVtable2 = new System.Windows.Forms.DataGridView();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.DeleteBT = new System.Windows.Forms.Button();
            this.SaveBTN = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.UsPriseTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UsNameTB = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LOGOlbl = new System.Windows.Forms.Label();
            this.BackClickBtn = new System.Windows.Forms.PictureBox();
            this.ExitBTN = new System.Windows.Forms.PictureBox();
            this.SeachTB = new System.Windows.Forms.TextBox();
            this.diagram1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.diagram3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.diagram2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.UsTypeTB = new System.Windows.Forms.ComboBox();
            this.balanceLBL = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGVtable2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BackClickBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitBTN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagram3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagram2)).BeginInit();
            this.SuspendLayout();
            // 
            // UsDateTB
            // 
            this.UsDateTB.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.UsDateTB.Location = new System.Drawing.Point(1350, 100);
            this.UsDateTB.Name = "UsDateTB";
            this.UsDateTB.Size = new System.Drawing.Size(150, 35);
            this.UsDateTB.TabIndex = 31;
            this.UsDateTB.Enter += new System.EventHandler(this.UsDateTB_Enter);
            this.UsDateTB.Leave += new System.EventHandler(this.UsDateTB_Leave);
            // 
            // Date
            // 
            this.Date.AutoSize = true;
            this.Date.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.Date.ForeColor = System.Drawing.Color.Green;
            this.Date.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Date.Location = new System.Drawing.Point(1275, 100);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(54, 29);
            this.Date.TabIndex = 29;
            this.Date.Text = "Date";
            // 
            // UsQuantityTB
            // 
            this.UsQuantityTB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UsQuantityTB.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.UsQuantityTB.FormattingEnabled = true;
            this.UsQuantityTB.Location = new System.Drawing.Point(1050, 98);
            this.UsQuantityTB.Name = "UsQuantityTB";
            this.UsQuantityTB.Size = new System.Drawing.Size(209, 37);
            this.UsQuantityTB.TabIndex = 27;
            // 
            // DGVtable2
            // 
            this.DGVtable2.AllowUserToAddRows = false;
            this.DGVtable2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Green;
            this.DGVtable2.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.DGVtable2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVtable2.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVtable2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.DGVtable2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVtable2.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVtable2.DefaultCellStyle = dataGridViewCellStyle7;
            this.DGVtable2.Location = new System.Drawing.Point(439, 534);
            this.DGVtable2.Name = "DGVtable2";
            this.DGVtable2.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVtable2.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.DGVtable2.Size = new System.Drawing.Size(1061, 247);
            this.DGVtable2.TabIndex = 25;
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold);
            this.UserNameLbl.ForeColor = System.Drawing.Color.Green;
            this.UserNameLbl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.UserNameLbl.Location = new System.Drawing.Point(772, 45);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(106, 37);
            this.UserNameLbl.TabIndex = 4;
            this.UserNameLbl.Text = "Users";
            // 
            // DeleteBT
            // 
            this.DeleteBT.BackColor = System.Drawing.Color.Green;
            this.DeleteBT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DeleteBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteBT.Font = new System.Drawing.Font("Arial", 15.75F);
            this.DeleteBT.ForeColor = System.Drawing.Color.White;
            this.DeleteBT.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.DeleteBT.Location = new System.Drawing.Point(215, 610);
            this.DeleteBT.Name = "DeleteBT";
            this.DeleteBT.Size = new System.Drawing.Size(150, 35);
            this.DeleteBT.TabIndex = 15;
            this.DeleteBT.Text = "Delete";
            this.DeleteBT.UseVisualStyleBackColor = false;
            this.DeleteBT.Click += new System.EventHandler(this.DeleteBT_Click);
            // 
            // SaveBTN
            // 
            this.SaveBTN.BackColor = System.Drawing.Color.Green;
            this.SaveBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SaveBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveBTN.Font = new System.Drawing.Font("Arial", 15.75F);
            this.SaveBTN.ForeColor = System.Drawing.Color.White;
            this.SaveBTN.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SaveBTN.Location = new System.Drawing.Point(750, 152);
            this.SaveBTN.Name = "SaveBTN";
            this.SaveBTN.Size = new System.Drawing.Size(150, 35);
            this.SaveBTN.TabIndex = 13;
            this.SaveBTN.Text = "Save";
            this.SaveBTN.UseVisualStyleBackColor = false;
            this.SaveBTN.Click += new System.EventHandler(this.SaveBTN_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(626, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 29);
            this.label5.TabIndex = 10;
            this.label5.Text = "Prise (UAH)";
            // 
            // UsPriseTB
            // 
            this.UsPriseTB.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.UsPriseTB.Location = new System.Drawing.Point(750, 98);
            this.UsPriseTB.Name = "UsPriseTB";
            this.UsPriseTB.Size = new System.Drawing.Size(150, 35);
            this.UsPriseTB.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label4.Location = new System.Drawing.Point(950, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 29);
            this.label4.TabIndex = 8;
            this.label4.Text = "Category";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label3.Location = new System.Drawing.Point(50, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 29);
            this.label3.TabIndex = 6;
            this.label3.Text = "Name";
            // 
            // UsNameTB
            // 
            this.UsNameTB.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.UsNameTB.Location = new System.Drawing.Point(150, 100);
            this.UsNameTB.Name = "UsNameTB";
            this.UsNameTB.Size = new System.Drawing.Size(150, 35);
            this.UsNameTB.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Green;
            this.panel1.Controls.Add(this.LOGOlbl);
            this.panel1.Controls.Add(this.BackClickBtn);
            this.panel1.Controls.Add(this.ExitBTN);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1560, 40);
            this.panel1.TabIndex = 32;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // LOGOlbl
            // 
            this.LOGOlbl.AutoSize = true;
            this.LOGOlbl.BackColor = System.Drawing.Color.Green;
            this.LOGOlbl.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold);
            this.LOGOlbl.ForeColor = System.Drawing.Color.White;
            this.LOGOlbl.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LOGOlbl.Location = new System.Drawing.Point(652, 0);
            this.LOGOlbl.Name = "LOGOlbl";
            this.LOGOlbl.Size = new System.Drawing.Size(262, 37);
            this.LOGOlbl.TabIndex = 3;
            this.LOGOlbl.Text = "MoneyControler";
            this.LOGOlbl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LOGOlbl_MouseDown);
            this.LOGOlbl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LOGOlbl_MouseMove);
            // 
            // BackClickBtn
            // 
            this.BackClickBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackClickBtn.Image = ((System.Drawing.Image)(resources.GetObject("BackClickBtn.Image")));
            this.BackClickBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.BackClickBtn.Location = new System.Drawing.Point(0, 0);
            this.BackClickBtn.Name = "BackClickBtn";
            this.BackClickBtn.Size = new System.Drawing.Size(40, 40);
            this.BackClickBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BackClickBtn.TabIndex = 26;
            this.BackClickBtn.TabStop = false;
            this.BackClickBtn.Click += new System.EventHandler(this.BackClickBtn_Click);
            this.BackClickBtn.MouseLeave += new System.EventHandler(this.BackClickBtn_MouseLeave);
            this.BackClickBtn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.BackClickBtn_MouseMove);
            // 
            // ExitBTN
            // 
            this.ExitBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitBTN.Image = ((System.Drawing.Image)(resources.GetObject("ExitBTN.Image")));
            this.ExitBTN.Location = new System.Drawing.Point(1520, 0);
            this.ExitBTN.Name = "ExitBTN";
            this.ExitBTN.Size = new System.Drawing.Size(40, 40);
            this.ExitBTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ExitBTN.TabIndex = 27;
            this.ExitBTN.TabStop = false;
            this.ExitBTN.Click += new System.EventHandler(this.ExitBTN_Click);
            this.ExitBTN.MouseLeave += new System.EventHandler(this.ExitBTN_MouseLeave);
            this.ExitBTN.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ExitBTN_MouseMove);
            // 
            // SeachTB
            // 
            this.SeachTB.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.SeachTB.Location = new System.Drawing.Point(185, 550);
            this.SeachTB.Name = "SeachTB";
            this.SeachTB.Size = new System.Drawing.Size(220, 35);
            this.SeachTB.TabIndex = 33;
            this.SeachTB.TextChanged += new System.EventHandler(this.SeachTB_TextChanged);
            // 
            // diagram1
            // 
            chartArea4.Name = "ChartArea1";
            this.diagram1.ChartAreas.Add(chartArea4);
            legend4.Name = "Legend1";
            this.diagram1.Legends.Add(legend4);
            this.diagram1.Location = new System.Drawing.Point(0, 193);
            this.diagram1.Name = "diagram1";
            this.diagram1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.diagram1.Series.Add(series4);
            this.diagram1.Size = new System.Drawing.Size(500, 300);
            this.diagram1.TabIndex = 34;
            this.diagram1.Text = "chart1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox2.Location = new System.Drawing.Point(115, 550);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(37, 37);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 35;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureBox1.Location = new System.Drawing.Point(722, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(37, 37);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // diagram3
            // 
            chartArea5.Name = "ChartArea1";
            this.diagram3.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.diagram3.Legends.Add(legend5);
            this.diagram3.Location = new System.Drawing.Point(1002, 193);
            this.diagram3.Name = "diagram3";
            this.diagram3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.diagram3.Series.Add(series5);
            this.diagram3.Size = new System.Drawing.Size(558, 300);
            this.diagram3.TabIndex = 36;
            this.diagram3.Text = "chart1";
            // 
            // diagram2
            // 
            chartArea6.Name = "ChartArea1";
            this.diagram2.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.diagram2.Legends.Add(legend6);
            this.diagram2.Location = new System.Drawing.Point(506, 193);
            this.diagram2.Name = "diagram2";
            this.diagram2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SeaGreen;
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.diagram2.Series.Add(series6);
            this.diagram2.Size = new System.Drawing.Size(500, 300);
            this.diagram2.TabIndex = 37;
            this.diagram2.Text = "chart1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(350, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 29);
            this.label1.TabIndex = 39;
            this.label1.Text = "Type";
            // 
            // UsTypeTB
            // 
            this.UsTypeTB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.UsTypeTB.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.UsTypeTB.FormattingEnabled = true;
            this.UsTypeTB.Items.AddRange(new object[] {
            "Income",
            "Expenses"});
            this.UsTypeTB.Location = new System.Drawing.Point(448, 100);
            this.UsTypeTB.Name = "UsTypeTB";
            this.UsTypeTB.Size = new System.Drawing.Size(150, 37);
            this.UsTypeTB.TabIndex = 40;
            this.UsTypeTB.SelectedIndexChanged += new System.EventHandler(this.UsTypeTB_SelectedIndexChanged);
            this.UsTypeTB.TextChanged += new System.EventHandler(this.UsTypeTB_TextChanged);
            // 
            // balanceLBL
            // 
            this.balanceLBL.AutoSize = true;
            this.balanceLBL.Font = new System.Drawing.Font("Arial Narrow", 18F);
            this.balanceLBL.ForeColor = System.Drawing.Color.Green;
            this.balanceLBL.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.balanceLBL.Location = new System.Drawing.Point(717, 496);
            this.balanceLBL.Name = "balanceLBL";
            this.balanceLBL.Size = new System.Drawing.Size(65, 29);
            this.balanceLBL.TabIndex = 41;
            this.balanceLBL.Text = "label1";
            // 
            // Workplace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1560, 793);
            this.Controls.Add(this.balanceLBL);
            this.Controls.Add(this.UsTypeTB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.diagram2);
            this.Controls.Add(this.diagram3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.diagram1);
            this.Controls.Add(this.SeachTB);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.DGVtable2);
            this.Controls.Add(this.UsDateTB);
            this.Controls.Add(this.SaveBTN);
            this.Controls.Add(this.DeleteBT);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.UsQuantityTB);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.UsNameTB);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.UsPriseTB);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Workplace";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Workplace";
            this.MouseEnter += new System.EventHandler(this.Workplace_MouseEnter);
            ((System.ComponentModel.ISupportInitialize)(this.DGVtable2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BackClickBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitBTN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagram3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagram2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button DeleteBT;
        private System.Windows.Forms.Button SaveBTN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox UsPriseTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UsNameTB;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.DataGridView DGVtable2;
        private System.Windows.Forms.PictureBox BackClickBtn;
        private System.Windows.Forms.ComboBox UsQuantityTB;
        private System.Windows.Forms.PictureBox ExitBTN;
        private System.Windows.Forms.Label Date;
        private System.Windows.Forms.TextBox UsDateTB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LOGOlbl;
        private System.Windows.Forms.TextBox SeachTB;
        private System.Windows.Forms.DataVisualization.Charting.Chart diagram1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DataVisualization.Charting.Chart diagram3;
        private System.Windows.Forms.DataVisualization.Charting.Chart diagram2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox UsTypeTB;
        private System.Windows.Forms.Label balanceLBL;
    }
}