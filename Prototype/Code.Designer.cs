﻿namespace Prototype
{
    partial class Code
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Code));
            this.label4 = new System.Windows.Forms.Label();
            this.CodeUsTB = new System.Windows.Forms.TextBox();
            this.LoginBT = new System.Windows.Forms.Button();
            this.eyePB = new System.Windows.Forms.PictureBox();
            this.ExitBTN = new System.Windows.Forms.PictureBox();
            this.BackClickBtn = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.eyePB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitBTN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClickBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Location = new System.Drawing.Point(75, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(278, 37);
            this.label4.TabIndex = 26;
            this.label4.Text = "Write the Admin code:";
            // 
            // CodeUsTB
            // 
            this.CodeUsTB.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CodeUsTB.Location = new System.Drawing.Point(82, 97);
            this.CodeUsTB.Name = "CodeUsTB";
            this.CodeUsTB.Size = new System.Drawing.Size(252, 35);
            this.CodeUsTB.TabIndex = 25;
            this.CodeUsTB.UseSystemPasswordChar = true;
            // 
            // LoginBT
            // 
            this.LoginBT.BackColor = System.Drawing.Color.Green;
            this.LoginBT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LoginBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LoginBT.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginBT.ForeColor = System.Drawing.Color.White;
            this.LoginBT.Location = new System.Drawing.Point(109, 150);
            this.LoginBT.Name = "LoginBT";
            this.LoginBT.Size = new System.Drawing.Size(198, 32);
            this.LoginBT.TabIndex = 24;
            this.LoginBT.Text = "LOGIN";
            this.LoginBT.UseVisualStyleBackColor = false;
            this.LoginBT.Click += new System.EventHandler(this.LoginBT_Click);
            // 
            // eyePB
            // 
            this.eyePB.Cursor = System.Windows.Forms.Cursors.Hand;
            this.eyePB.Image = global::Prototype.Properties.Resources.eye_slash;
            this.eyePB.Location = new System.Drawing.Point(353, 97);
            this.eyePB.Name = "eyePB";
            this.eyePB.Size = new System.Drawing.Size(35, 35);
            this.eyePB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.eyePB.TabIndex = 30;
            this.eyePB.TabStop = false;
            this.eyePB.Click += new System.EventHandler(this.eyePB_Click);
            // 
            // ExitBTN
            // 
            this.ExitBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitBTN.Image = ((System.Drawing.Image)(resources.GetObject("ExitBTN.Image")));
            this.ExitBTN.Location = new System.Drawing.Point(360, 0);
            this.ExitBTN.Name = "ExitBTN";
            this.ExitBTN.Size = new System.Drawing.Size(40, 40);
            this.ExitBTN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitBTN.TabIndex = 29;
            this.ExitBTN.TabStop = false;
            this.ExitBTN.Click += new System.EventHandler(this.ExitBTN_Click);
            this.ExitBTN.MouseLeave += new System.EventHandler(this.ExitBTN_MouseLeave);
            this.ExitBTN.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ExitBTN_MouseMove);
            // 
            // BackClickBtn
            // 
            this.BackClickBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BackClickBtn.Image = ((System.Drawing.Image)(resources.GetObject("BackClickBtn.Image")));
            this.BackClickBtn.Location = new System.Drawing.Point(0, 0);
            this.BackClickBtn.Name = "BackClickBtn";
            this.BackClickBtn.Size = new System.Drawing.Size(40, 40);
            this.BackClickBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BackClickBtn.TabIndex = 28;
            this.BackClickBtn.TabStop = false;
            this.BackClickBtn.Click += new System.EventHandler(this.BackClickBtn_Click);
            this.BackClickBtn.MouseLeave += new System.EventHandler(this.BackClickBtn_MouseLeave);
            this.BackClickBtn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.BackClickBtn_MouseMove);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Prototype.Properties.Resources.key;
            this.pictureBox3.Location = new System.Drawing.Point(28, 97);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(35, 35);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Green;
            this.panel1.Controls.Add(this.ExitBTN);
            this.panel1.Controls.Add(this.BackClickBtn);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 40);
            this.panel1.TabIndex = 31;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // Code
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(400, 200);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.eyePB);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CodeUsTB);
            this.Controls.Add(this.LoginBT);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Code";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Code";
            ((System.ComponentModel.ISupportInitialize)(this.eyePB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExitBTN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClickBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox CodeUsTB;
        private System.Windows.Forms.Button LoginBT;
        private System.Windows.Forms.PictureBox BackClickBtn;
        private System.Windows.Forms.PictureBox ExitBTN;
        private System.Windows.Forms.PictureBox eyePB;
        private System.Windows.Forms.Panel panel1;
    }
}