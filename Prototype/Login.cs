﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing;
namespace Prototype
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        public static string UserName = "";
        private void AdminBTN_Click(object sender, EventArgs e)
        {
            Code use = new Code();
            use.Show();
            this.Hide();
        }
        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True");
        private void LoginBTN_Click(object sender, EventArgs e)
        {
            connection.Open();

            SqlDataAdapter ada = new SqlDataAdapter("select count(*) from UsersTbl where UsName COLLATE Latin1_General_CS_AS = '" + NameUsTB.Text + "' and UsPass = '" + PasswordUsTB.Text + "' ", connection);

            DataTable dt = new DataTable();
            ada.Fill(dt);

            if (dt.Rows[0][0].ToString() == "1")
            {
                UserName = NameUsTB.Text;

                Workplace wp = new Workplace();
                wp.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Wrong UserName or Password");
            }

            connection.Close();
        }
        private void ExitBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void RegBTN_Click(object sender, EventArgs e)
        {
            Registration reg = new Registration();
            reg.Show();
            this.Hide();
        }
        private bool isImage1 = true;
        private void eyePB_Click(object sender, EventArgs e)
        {
            if (isImage1)
            {
                eyePB.Image = Properties.Resources.eye;
                PasswordUsTB.UseSystemPasswordChar = false;
            }
            else
            {
                eyePB.Image = Properties.Resources.eye_slash;
                PasswordUsTB.UseSystemPasswordChar = true;
            }
            isImage1 = !isImage1;
        }
        Point lastPoint;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void LOGOlbl_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void LOGOlbl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void ExitBTN_MouseMove(object sender, MouseEventArgs e)
        {
            ExitBTN.BackColor = Color.Red;
        }
        private void ExitBTN_MouseLeave(object sender, EventArgs e)
        {
            ExitBTN.BackColor = Color.Green;
        }
    }
}
