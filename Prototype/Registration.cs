﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
namespace Prototype
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
            populate();
        }
        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True");
        private void populate()
        {
            connection.Open();
            string query = "select * from UsersTbl";
            SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
            SqlCommandBuilder builder = new SqlCommandBuilder(adapter);
            var ds = new DataSet();
            adapter.Fill(ds);
            connection.Close();
        }
        private bool IsPhoneNumberValid(string phoneNumber)
        {
            string pattern = @"(((\+380)[- ]?)|0)[35679][0-9]((\d{7})|(\d{6})|(-\d{2}-\d{2}-\d{3})|(-\d{3}-\d{2}-\d{2})|(-\d{3}-\d{4})|(-\d{2}-\d{2}-\d{2}-\d)|(\d-\d{2}-\d{2}-\d{2}))";
            return Regex.IsMatch(phoneNumber, pattern);
        }
        private void LoginBT_Click(object sender, EventArgs e)
        {
            if (NameUsTB.Text == "" || PasswordUsTB.Text == "" || PhoneUsTB.Text == "")

                MessageBox.Show("Missing Information");
            else if (!IsPhoneNumberValid(PhoneUsTB.Text))
            {
                MessageBox.Show("Phone number is not valid");
            }
            else
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM UsersTbl WHERE UsName = '" + NameUsTB.Text + "' OR UsPhone = '" + PhoneUsTB.Text + "'", connection);
                    int count = (int)cmd.ExecuteScalar();

                    if (count > 0)
                    {
                        MessageBox.Show("User already exists");
                        connection.Close();
                    }
                    else
                    {
                        cmd = new SqlCommand("INSERT INTO UsersTbl VALUES ('" + NameUsTB.Text + "','" + PasswordUsTB.Text + "','" + PhoneUsTB.Text + "')", connection);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("User saved successfully");
                        NameUsTB.Text = string.Empty;
                        PasswordUsTB.Text = string.Empty;
                        PhoneUsTB.Text = string.Empty;
                        connection.Close();
                        populate();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void BackClickBtn_Click(object sender, EventArgs e)
        {
            Login use = new Login();
            use.Show();
            this.Hide();
        }
        private void ExitBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool isImage1 = true;
        private void eyePB_Click(object sender, EventArgs e)
        {
            if (isImage1)
            {
                eyePB.Image = Properties.Resources.eye;
                PasswordUsTB.UseSystemPasswordChar = false;
            }
            else
            {
                eyePB.Image = Properties.Resources.eye_slash;
                PasswordUsTB.UseSystemPasswordChar = true;
            }
            isImage1 = !isImage1;
        }
        Point lastPoint;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void ExitBTN_MouseMove(object sender, MouseEventArgs e)
        {
            ExitBTN.BackColor = Color.Red;
        }
        private void ExitBTN_MouseLeave(object sender, EventArgs e)
        {
            ExitBTN.BackColor = Color.Green;
        }
        private void BackClickBtn_MouseMove(object sender, MouseEventArgs e)
        {
            BackClickBtn.BackColor = Color.Aquamarine;
        }
        private void BackClickBtn_MouseLeave(object sender, EventArgs e)
        {
            BackClickBtn.BackColor = Color.Green;
        }
    }
}
