﻿namespace Prototype
{
    partial class Users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Users));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExitBTN = new System.Windows.Forms.PictureBox();
            this.LOGOlbl = new System.Windows.Forms.Label();
            this.BackClickBtn = new System.Windows.Forms.PictureBox();
            this.DeleteBTN = new System.Windows.Forms.Button();
            this.DGVtable = new System.Windows.Forms.DataGridView();
            this.SaveBTN = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.UsPasswordTB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.UsPhoneTB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.UsNameTB = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitBTN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClickBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVtable)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Green;
            this.panel1.Controls.Add(this.ExitBTN);
            this.panel1.Controls.Add(this.LOGOlbl);
            this.panel1.Controls.Add(this.BackClickBtn);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // ExitBTN
            // 
            this.ExitBTN.BackColor = System.Drawing.Color.Green;
            this.ExitBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.ExitBTN, "ExitBTN");
            this.ExitBTN.Name = "ExitBTN";
            this.ExitBTN.TabStop = false;
            this.ExitBTN.Click += new System.EventHandler(this.ExitBTN_Click);
            this.ExitBTN.MouseLeave += new System.EventHandler(this.ExitBTN_MouseLeave);
            this.ExitBTN.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ExitBTN_MouseMove);
            // 
            // LOGOlbl
            // 
            resources.ApplyResources(this.LOGOlbl, "LOGOlbl");
            this.LOGOlbl.BackColor = System.Drawing.Color.Green;
            this.LOGOlbl.ForeColor = System.Drawing.Color.White;
            this.LOGOlbl.Name = "LOGOlbl";
            this.LOGOlbl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LOGOlbl_MouseDown);
            this.LOGOlbl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LOGOlbl_MouseMove);
            // 
            // BackClickBtn
            // 
            this.BackClickBtn.BackColor = System.Drawing.Color.Green;
            this.BackClickBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.BackClickBtn, "BackClickBtn");
            this.BackClickBtn.Name = "BackClickBtn";
            this.BackClickBtn.TabStop = false;
            this.BackClickBtn.Click += new System.EventHandler(this.BackClickBtn_Click);
            this.BackClickBtn.MouseLeave += new System.EventHandler(this.BackClickBtn_MouseLeave);
            this.BackClickBtn.MouseMove += new System.Windows.Forms.MouseEventHandler(this.BackClickBtn_MouseMove);
            // 
            // DeleteBTN
            // 
            this.DeleteBTN.BackColor = System.Drawing.Color.Green;
            this.DeleteBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.DeleteBTN, "DeleteBTN");
            this.DeleteBTN.ForeColor = System.Drawing.Color.White;
            this.DeleteBTN.Name = "DeleteBTN";
            this.DeleteBTN.UseVisualStyleBackColor = false;
            this.DeleteBTN.Click += new System.EventHandler(this.DeleteBTN_Click);
            // 
            // DGVtable
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Green;
            this.DGVtable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.DGVtable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DGVtable.BackgroundColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVtable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.DGVtable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVtable.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVtable.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.DGVtable, "DGVtable");
            this.DGVtable.Name = "DGVtable";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Green;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVtable.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            // 
            // SaveBTN
            // 
            this.SaveBTN.BackColor = System.Drawing.Color.Green;
            this.SaveBTN.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.SaveBTN, "SaveBTN");
            this.SaveBTN.ForeColor = System.Drawing.Color.White;
            this.SaveBTN.Name = "SaveBTN";
            this.SaveBTN.UseVisualStyleBackColor = false;
            this.SaveBTN.Click += new System.EventHandler(this.SaveBTN_Click);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.Green;
            this.label6.Name = "label6";
            // 
            // UsPasswordTB
            // 
            resources.ApplyResources(this.UsPasswordTB, "UsPasswordTB");
            this.UsPasswordTB.Name = "UsPasswordTB";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.Green;
            this.label4.Name = "label4";
            // 
            // UsPhoneTB
            // 
            resources.ApplyResources(this.UsPhoneTB, "UsPhoneTB");
            this.UsPhoneTB.Name = "UsPhoneTB";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Green;
            this.label3.Name = "label3";
            // 
            // UsNameTB
            // 
            resources.ApplyResources(this.UsNameTB, "UsNameTB");
            this.UsNameTB.Name = "UsNameTB";
            // 
            // Users
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.SaveBTN);
            this.Controls.Add(this.DeleteBTN);
            this.Controls.Add(this.DGVtable);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.UsPhoneTB);
            this.Controls.Add(this.UsPasswordTB);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UsNameTB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Users";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitBTN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackClickBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGVtable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LOGOlbl;
        private System.Windows.Forms.TextBox UsNameTB;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox UsPasswordTB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox UsPhoneTB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button SaveBTN;
        private System.Windows.Forms.DataGridView DGVtable;
        private System.Windows.Forms.PictureBox BackClickBtn;
        private System.Windows.Forms.Button DeleteBTN;
        private System.Windows.Forms.PictureBox ExitBTN;
    }
}