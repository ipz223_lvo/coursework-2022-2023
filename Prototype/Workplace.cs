﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Prototype
{
    public partial class Workplace : Form
    {
        public Workplace()
        {
            InitializeComponent();
            UserNameLbl.Text = Login.UserName;
            populate();
            ShowExpensesChart();
            ShowIncomeChart();
            ShowComparisonChart();
            UsDateTB.Text = "dd-mm-yyyy";
            UsDateTB.ForeColor = Color.Gray;
        }
        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True");
        private void populate()
        {
            connection.Open();
            string query = "select  WName, WType, WPrise, WCategory, WData from WorkTable where WIndentificator = @Identifier";

            SqlDataAdapter adapter = new SqlDataAdapter(query, connection);
            adapter.SelectCommand.Parameters.AddWithValue("@Identifier", UserNameLbl.Text);
            SqlCommandBuilder builder2 = new SqlCommandBuilder(adapter);
            var ds = new DataSet();
            adapter.Fill(ds);
            DGVtable2.DataSource = ds.Tables[0];
            connection.Close();
        }       
        private void Workplace_MouseEnter(object sender, EventArgs e)
        {
            UserNameLbl.Text = Login.UserName;
        }
        private void SaveBTN_Click(object sender, EventArgs e)
        {
            string priceInput = UsPriseTB.Text.Trim();
            string input = UsDateTB.Text.Trim();



            if (UsNameTB.Text == "" || UsQuantityTB.Text == "" || UsPriseTB.Text == "" || UsDateTB.Text == "" || UsTypeTB.Text == "")
            {
                MessageBox.Show("Missing Information");
            }
            else if (!Regex.IsMatch(priceInput, @"^\d+$"))
            {
                MessageBox.Show("Invalid Price. Please enter only digits.");
                UsPriseTB.Text = string.Empty;
                UsPriseTB.Focus();

            }
            else if (!Regex.IsMatch(input, @"^(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-(\d{4})$"))
            {
                MessageBox.Show("Wrong date format! Enter the date in the format dd-mm-yyyy");
                UsDateTB.Text = string.Empty;
                UsDateTB.Focus();
            }
            else
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("Insert into WorkTable values ('" + UsNameTB.Text + "','" + UsQuantityTB.Text + "','" + UsPriseTB.Text + "','" + UserNameLbl.Text + "','" + UsDateTB.Text + "','" + UsTypeTB.Text + "')", connection);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Item Saved Successfully");
                    connection.Close();
                    populate();
                    ShowExpensesChart();
                    ShowIncomeChart();
                    ShowComparisonChart();
                    UsNameTB.Text = string.Empty;
                    UsQuantityTB.Text = string.Empty;
                    UsPriseTB.Text = string.Empty;
                    UsDateTB.Text = string.Empty;
                    UsQuantityTB.Items.Clear();
                    UsTypeTB.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void BackClickBtn_Click(object sender, EventArgs e)
        {
            Login use = new Login();
            use.Show();
            this.Hide();
        }
        private void ExitBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void DeleteBT_Click(object sender, EventArgs e)
        {
            if (DGVtable2.SelectedRows.Count > 0)
            {
                int selectedIndex = DGVtable2.SelectedRows[0].Index;
                string selectedWName = DGVtable2.SelectedRows[0].Cells["WName"].Value.ToString();

                DialogResult result = MessageBox.Show("Are you sure you want to delete the entry '" + selectedWName + "'?", "Confirm delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        string selectIdQuery = "SELECT Id FROM WorkTable WHERE WName = @WName";
                        SqlCommand selectIdCommand = new SqlCommand(selectIdQuery, connection);
                        selectIdCommand.Parameters.AddWithValue("@WName", selectedWName);

                        connection.Open();

                        int selectedId = Convert.ToInt32(selectIdCommand.ExecuteScalar());

                        string deleteQuery = "DELETE FROM WorkTable WHERE Id = @Id";
                        SqlCommand deleteCommand = new SqlCommand(deleteQuery, connection);
                        deleteCommand.Parameters.AddWithValue("@Id", selectedId);

                        deleteCommand.ExecuteNonQuery();
                    }

                    populate();
                    ShowExpensesChart();
                    ShowIncomeChart();
                    ShowComparisonChart();
                }
            }
            else
            {
                MessageBox.Show("Select the line you want to delete.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        Point lastPoint;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void LOGOlbl_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void LOGOlbl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void ExitBTN_MouseMove(object sender, MouseEventArgs e)
        {
            ExitBTN.BackColor = Color.Red;
        }
        private void ExitBTN_MouseLeave(object sender, EventArgs e)
        {
            ExitBTN.BackColor = Color.Green;
        }
        private void BackClickBtn_MouseMove(object sender, MouseEventArgs e)
        {
            BackClickBtn.BackColor = Color.Aquamarine;
        }
        private void BackClickBtn_MouseLeave(object sender, EventArgs e)
        {
            BackClickBtn.BackColor = Color.Green;
        }

        string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\Вадим\\Desktop\\Prototype\\Prototype\\Database1.mdf;Integrated Security=True";
        private void SeachTB_TextChanged(object sender, EventArgs e)
        {
            string TEXT = SeachTB.Text;

            string query = "SELECT WName, WPrise, WCategory, WData FROM WorkTable WHERE WName LIKE '%' + @TEXT + '%' OR WData LIKE '%' + @TEXT + '%'";

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@TEXT", TEXT);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                DGVtable2.DataSource = dataTable;
            }
        }
        private void Workplace_Load(object sender, EventArgs e)
        {
            populate();
        }
        private void UsTypeTB_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedType = UsTypeTB.Text;
            if (selectedType == "Income")
            {
                UsQuantityTB.Items.Clear();
                UsQuantityTB.Text = string.Empty;
                UsQuantityTB.Items.Add("Salary");
                UsQuantityTB.Items.Add("Dividends");
                UsQuantityTB.Items.Add("Renting out real estate");
                UsQuantityTB.Items.Add("Gifts and legacy");
                UsQuantityTB.Items.Add("Investments");
                UsQuantityTB.Items.Add("Sale of property");
                UsQuantityTB.Items.Add("Other");
                if (selectedType != "Income")
                {
                    UsQuantityTB.Items.Clear();
                    UsQuantityTB.Text = string.Empty;
                }
            }
            else if (selectedType == "Expenses")
            {
                UsQuantityTB.Items.Clear();
                UsQuantityTB.Text = string.Empty;
                UsQuantityTB.Items.Add("Food");
                UsQuantityTB.Items.Add("Transport");
                UsQuantityTB.Items.Add("Housing and utilities");
                UsQuantityTB.Items.Add("Health and medicine");
                UsQuantityTB.Items.Add("Entertainment");
                UsQuantityTB.Items.Add("Clothing");
                UsQuantityTB.Items.Add("Trips");
                UsQuantityTB.Items.Add("Other");
            }
            else
            {
                UsQuantityTB.Items.Clear();
            }
        }

        private void UsTypeTB_TextChanged(object sender, EventArgs e)
        {
            string selectedType = UsTypeTB.Text;

            if (selectedType != "Income" && selectedType != "Expenses")
            {
                UsTypeTB.Text = "";
            }
        }

        private void ShowExpensesChart()
        {
            diagram1.Series.Clear();
            diagram1.Titles.Clear();

            DataTable dataTable = (DataTable)DGVtable2.DataSource;
            DataRow[] expenseRows = dataTable.Select("WType = 'Expenses'");

            Series series = new Series("Expenses");
            series.ChartType = SeriesChartType.Pie;

            Dictionary<string, double> categoryExpenses = new Dictionary<string, double>();
            foreach (DataRow row in expenseRows)
            {
                string category = row["WCategory"].ToString();
                double amount = Convert.ToDouble(row["WPrise"]);

                if (categoryExpenses.ContainsKey(category))
                {
                    categoryExpenses[category] += amount;
                }
                else
                {
                    categoryExpenses[category] = amount;
                }
            }

            double totalExpenses = categoryExpenses.Values.Sum();
            foreach (var kvp in categoryExpenses)
            {
                string category = kvp.Key;
                double amount = kvp.Value;
                double percentage = (amount / totalExpenses) * 100;

                series.Points.AddXY("", amount);
                series.Points.Last().LegendText = $"{category} ({percentage:F2}%)";
            }

            if (categoryExpenses.Count > 0)
            {
                diagram1.Series.Add(series);
                diagram1.Titles.Add("Expenses Chart");
                diagram1.ChartAreas[0].Area3DStyle.Enable3D = true;
                diagram1.Legends[0].Enabled = true;
                diagram1.Legends[0].Docking = Docking.Right;
                diagram1.Series[0].IsValueShownAsLabel = true;
                diagram1.Series[0].LabelFormat = "#,##0.00";

                diagram1.Legends[0].Font = new Font(diagram1.Legends[0].Font.FontFamily, 12f);
                diagram1.Series[0].Font = new Font(diagram1.Series[0].Font.FontFamily, 12f);
                diagram1.Titles[0].Font = new Font("Arial", 14, FontStyle.Bold);

                diagram1.ChartAreas[0].AxisX.LabelStyle.Enabled = false;
                diagram1.ChartAreas[0].AxisY.LabelStyle.Enabled = false;
            }
            else
            {
                diagram1.Series.Add(series);
                diagram1.Titles.Add("No Data");
                diagram1.ChartAreas[0].Area3DStyle.Enable3D = false;
                diagram1.Legends[0].Enabled = false;
                diagram1.Series[0].IsValueShownAsLabel = false;

                diagram1.Series[0].Points.Clear();
                diagram1.Series[0].Points.Add(100);
                diagram1.Series[0].Points[0].Color = Color.Gray;
                diagram1.Series[0].Points[0].LegendText = "";
                diagram1.Titles[0].Font = new Font("Arial", 14, FontStyle.Bold);
            }
        }

        private void ShowIncomeChart()
        {
            diagram3.Series.Clear();
            diagram3.Titles.Clear();

            DataTable dataTable = (DataTable)DGVtable2.DataSource;
            DataRow[] incomeRows = dataTable.Select("WType = 'Income'");

            Series series = new Series("Income");
            series.ChartType = SeriesChartType.Pie;

            Dictionary<string, double> categoryIncome = new Dictionary<string, double>();
            foreach (DataRow row in incomeRows)
            {
                string category = row["WCategory"].ToString();
                double amount = Convert.ToDouble(row["WPrise"]);

                if (categoryIncome.ContainsKey(category))
                {
                    categoryIncome[category] += amount;
                }
                else
                {
                    categoryIncome[category] = amount;
                }
            }

            double totalIncome = categoryIncome.Values.Sum();
            foreach (var kvp in categoryIncome)
            {
                string category = kvp.Key;
                double amount = kvp.Value;
                double percentage = (amount / totalIncome) * 100;

                series.Points.AddXY("", amount);
                series.Points.Last().LegendText = $"{category} ({percentage:F2}%)";
            }

            if (categoryIncome.Count > 0)
            {
                diagram3.Series.Add(series);
                diagram3.Titles.Add("Income Chart");
                diagram3.ChartAreas[0].Area3DStyle.Enable3D = true;
                diagram3.Legends[0].Enabled = true;
                diagram3.Legends[0].Docking = Docking.Right;
                diagram3.Series[0].IsValueShownAsLabel = true;
                diagram3.Series[0].LabelFormat = "#,##0.00";

                diagram3.Legends[0].Font = new Font(diagram3.Legends[0].Font.FontFamily, 12f);
                diagram3.Series[0].Font = new Font(diagram3.Series[0].Font.FontFamily, 12f);
                diagram3.Titles[0].Font = new Font("Arial", 14, FontStyle.Bold);

                diagram3.ChartAreas[0].AxisX.LabelStyle.Enabled = false;
                diagram3.ChartAreas[0].AxisY.LabelStyle.Enabled = false;
            }
            else
            {
                diagram3.Series.Add(series);
                diagram3.Titles.Add("No Data");
                diagram3.ChartAreas[0].Area3DStyle.Enable3D = false;
                diagram3.Legends[0].Enabled = false;
                diagram3.Series[0].IsValueShownAsLabel = false;

                diagram3.Series[0].Points.Clear();
                diagram3.Series[0].Points.Add(100);
                diagram3.Series[0].Points[0].Color = Color.Gray;
                diagram3.Series[0].Points[0].LegendText = "";
                diagram3.Titles[0].Font = new Font("Arial", 14, FontStyle.Bold);
            }
        }
        private void ShowComparisonChart()
        {
            diagram2.Series.Clear();
            diagram2.Titles.Clear();
            DataTable dataTable = (DataTable)DGVtable2.DataSource;
            DataRow[] expenseRows = dataTable.Select("WType = 'Expenses'");
            DataRow[] incomeRows = dataTable.Select("WType = 'Income'");

            double totalExpenses = CalculateTotalAmount(expenseRows);
            double totalIncome = CalculateTotalAmount(incomeRows);

            double balance = totalIncome - totalExpenses;  // Обчислення балансу

            Series series = new Series("Comparison");
            series.ChartType = SeriesChartType.Pie;

            series.Points.AddXY("Expenses", totalExpenses);
            series.Points.AddXY("Income", totalIncome);

            double percentageExpenses = (totalExpenses / (totalExpenses + totalIncome)) * 100;
            double percentageIncome = (totalIncome / (totalExpenses + totalIncome)) * 100;

            series.Points[0].LegendText = $"Expenses ({percentageExpenses:F2}%)";
            series.Points[1].LegendText = $"Income ({percentageIncome:F2}%)";

            if (totalExpenses > 0 || totalIncome > 0)
            {
                diagram2.Series.Add(series);
                diagram2.Titles.Add("Comparison Chart");
                diagram2.ChartAreas[0].Area3DStyle.Enable3D = true;
                diagram2.Legends[0].Enabled = true;
                diagram2.Legends[0].Docking = Docking.Right;
                diagram2.Series[0].IsValueShownAsLabel = true;
                diagram2.Series[0].LabelFormat = "#,##0.00";

                diagram2.Legends[0].Font = new Font(diagram2.Legends[0].Font.FontFamily, 12f);
                diagram2.Series[0].Font = new Font(diagram2.Series[0].Font.FontFamily, 12f);
                diagram2.Titles[0].Font = new Font("Arial", 16, FontStyle.Bold);

                diagram2.ChartAreas[0].AxisX.LabelStyle.Enabled = false;
                diagram2.ChartAreas[0].AxisY.LabelStyle.Enabled = false;

                balanceLBL.Text = $"Balance: {balance:F2}";
            }
            else
            {
                diagram2.Series.Add(series);
                diagram2.Titles.Add("No Data");
                diagram2.ChartAreas[0].Area3DStyle.Enable3D = false;
                diagram2.Legends[0].Enabled = false;
                diagram2.Series[0].IsValueShownAsLabel = false;

                diagram2.Series[0].Points.Clear();
                diagram2.Series[0].Points.Add(100);
                diagram2.Series[0].Points[0].Color = Color.Gray;
                diagram2.Series[0].Points[0].LegendText = "";
                diagram2.Titles[0].Font = new Font("Arial", 16, FontStyle.Bold);

                balanceLBL.Text = "Balance: N/A";
            }
        }
        private double CalculateTotalAmount(DataRow[] rows)
        {
            double totalAmount = 0;
            foreach (DataRow row in rows)
            {
                double amount = Convert.ToDouble(row["WPrise"]);
                totalAmount += amount;
            }
            return totalAmount;
        }
        private void UsDateTB_Enter(object sender, EventArgs e)
        {
            if (UsDateTB.Text == "dd-mm-yyyy")
            {
                UsDateTB.Text = "";
                UsDateTB.ForeColor = Color.Black;
            }
        }

        private void UsDateTB_Leave(object sender, EventArgs e)
        {
            if (UsDateTB.Text == "")
            {
                UsDateTB.Text = "dd-mm-yyyy";
                UsDateTB.ForeColor = Color.Gray;
            }
        }
    }
}
