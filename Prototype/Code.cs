﻿using System;
using System.Drawing;
using System.Windows.Forms;
namespace Prototype
{
    public partial class Code : Form
    {
        public Code()
        {
            InitializeComponent();
        }
        private void LoginBT_Click(object sender, EventArgs e)
        {
            if (CodeUsTB.Text == "123456")
            {
                Users use = new Users();
                use.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Wrong Code");
        }
        private void BackClickBtn_Click(object sender, EventArgs e)
        {
            Login use = new Login();
            use.Show();
            this.Hide();
        }
        private void ExitBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool isImage1 = true;
        private void eyePB_Click(object sender, EventArgs e)
        {
            if (isImage1)
            {
                eyePB.Image = Properties.Resources.eye;
                CodeUsTB.UseSystemPasswordChar = false;
            }
            else
            {
                eyePB.Image = Properties.Resources.eye_slash;
                CodeUsTB.UseSystemPasswordChar = true;
            }
            isImage1 = !isImage1;
        }
        Point lastPoint;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }
        private void ExitBTN_MouseMove(object sender, MouseEventArgs e)
        {
            ExitBTN.BackColor = Color.Red;
        }
        private void ExitBTN_MouseLeave(object sender, EventArgs e)
        {
            ExitBTN.BackColor = Color.Green;
        }
        private void BackClickBtn_MouseMove(object sender, MouseEventArgs e)
        {
            BackClickBtn.BackColor = Color.Aquamarine;
        }
        private void BackClickBtn_MouseLeave(object sender, EventArgs e)
        {
            BackClickBtn.BackColor = Color.Green;
        }
    }
}
