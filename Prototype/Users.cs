﻿using System;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Drawing;

namespace Prototype
{
    public partial class Users : Form
    {
        public Users()
        {
            InitializeComponent();
            populate();
        }
        SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True");
        private void populate()
        {
            connection.Open();
            string query = "select UsName, UsPass, UsPhone  from UsersTbl";
            SqlDataAdapter adapter = new SqlDataAdapter(query,connection);
            SqlCommandBuilder builder = new SqlCommandBuilder(adapter);
            var ds = new DataSet();
            adapter.Fill(ds);
            DGVtable.DataSource = ds.Tables[0];
            connection.Close();
        }
        private void LoadData()
        {
            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True");
            {
                string query = "SELECT UsName, UsPass, UsPhone FROM UsersTbl";
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                DGVtable.DataSource = dataTable;
            }
        }
        private bool IsPhoneNumberValid(string phoneNumber)
        {
            string pattern = @"(((\+380)[- ]?)|0)[35679][0-9]((\d{7})|(\d{6})|(-\d{2}-\d{2}-\d{3})|(-\d{3}-\d{2}-\d{2})|(-\d{3}-\d{4})|(-\d{2}-\d{2}-\d{2}-\d)|(\d-\d{2}-\d{2}-\d{2}))";
            return Regex.IsMatch(phoneNumber, pattern);
        }
        private void SaveBTN_Click(object sender, EventArgs e)
        {
            if (UsNameTB.Text == "" || UsPhoneTB.Text == "" || UsPasswordTB.Text == "")
                MessageBox.Show("Missing Information");
            else if (!IsPhoneNumberValid(UsPhoneTB.Text))
            {
                MessageBox.Show("Phone number is not valid");
            }
            else
            {
                try
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM UsersTbl WHERE UsName = '" + UsNameTB.Text + "' OR UsPhone = '" + UsPhoneTB.Text + "'", connection);
                    int count = (int)cmd.ExecuteScalar();

                    if (count > 0)
                    {
                        MessageBox.Show("User already exists");
                        connection.Close();
                    }
                    else
                    {
                        cmd = new SqlCommand("INSERT INTO UsersTbl VALUES ('" + UsNameTB.Text + "','" + UsPasswordTB.Text + "','" + UsPhoneTB.Text + "')", connection);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("User saved successfully");
                        UsNameTB.Text = string.Empty;
                        UsPhoneTB.Text = string.Empty;
                        UsPasswordTB.Text = string.Empty;
                        connection.Close();
                        populate();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void DeleteBTN_Click(object sender, EventArgs e)
        {
            if (DGVtable.SelectedRows.Count > 0)
            {
                int selectedIndex = DGVtable.SelectedRows[0].Index;
                string selectedWName = DGVtable.SelectedRows[0].Cells["UsName"].Value.ToString();
                DialogResult result = MessageBox.Show("Are you sure you want to delete the user '" + selectedWName + "'?", "Confirm deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Вадим\Desktop\Prototype\Prototype\Database1.mdf;Integrated Security=True");
                    {
                        connection.Open();
                        // Видалення із таблиці UsersTbl
                        string deleteUsersQuery = "DELETE FROM UsersTbl WHERE UsName = @UsName";
                        SqlCommand deleteUsersCommand = new SqlCommand(deleteUsersQuery, connection);
                        deleteUsersCommand.Parameters.AddWithValue("@UsName", selectedWName);
                        deleteUsersCommand.ExecuteNonQuery();

                        // Видалення із таблиці WorkTable
                        string deleteWorkTableQuery = "DELETE FROM WorkTable WHERE WIndentificator = @UsName";
                        SqlCommand deleteWorkTableCommand = new SqlCommand(deleteWorkTableQuery, connection);
                        deleteWorkTableCommand.Parameters.AddWithValue("@UsName", selectedWName);
                        deleteWorkTableCommand.ExecuteNonQuery();
                        connection.Close();
                    }
                    LoadData();
                }
            }
            else
            {
                MessageBox.Show("Select the line you want to delete.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ExitBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void BackClickBtn_Click(object sender, EventArgs e)
        {
            Login use = new Login();
            use.Show();
            this.Close();
        }
        Point lastPoint;
        private void LOGOlbl_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void LOGOlbl_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void ExitBTN_MouseMove(object sender, MouseEventArgs e)
        {
            ExitBTN.BackColor = Color.Red;
        }

        private void ExitBTN_MouseLeave(object sender, EventArgs e)
        {
            ExitBTN.BackColor = Color.Green;
        }

        private void BackClickBtn_MouseMove(object sender, MouseEventArgs e)
        {
            BackClickBtn.BackColor = Color.Aquamarine;
        }

        private void BackClickBtn_MouseLeave(object sender, EventArgs e)
        {
            BackClickBtn.BackColor = Color.Green;
        }
    }
}
